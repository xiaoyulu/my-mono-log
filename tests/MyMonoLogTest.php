<?php
declare(strict_types=1);

namespace LiLei\Logs\Tests;

use PHPUnit\Framework\TestCase;
use LiLei\Logs\MyMonoLog;

class MyMonoLogTest extends TestCase
{
    // vendor/bin/phpunit tests/MyMonoLogTest.php --filter testWrite
    public function testWrite()
    {
        $myMonoLog = new MyMonoLog();
        $message   = __CLASS__. '->' . __FUNCTION__ .'()';
        $myMonoLog->write('test', $message);
    }// testWrite() end

    // vendor/bin/phpunit tests/MyMonoLogTest.php --filter testInfo
    public function testInfo()
    {
        $myMonoLog = new MyMonoLog();
        $message   = __CLASS__. '->' . __FUNCTION__ .'()';
        $myMonoLog->channel('test')->info($message);
    }// testInfo() end
}