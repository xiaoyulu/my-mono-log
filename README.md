# MyMonoLog



对 `monolog/monolog` 扩展包进行封装、调整，让其在项目里简单易用。


## 安装

[lilei/my-mono-log](https://packagist.org/packages/lilei/my-mono-log)

```shell
$ composer require lilei/my-mono-log
```



## 日志输出目录

`project_root/monolog/`，如果你需要修改的日志输出目录，请自行修改

`src\MyMonoLog.php->setDirname()`

```php
$MyMonoLog = new MyMonoLog();
$MyMonoLog->setDirname("my-dirname")->write(string $channel, string|array $message);
```





## package

使用到的扩展包：

- monolog/monolog
- phpunit/phpunit



## 测试

```shell
$ vendor/bin/phpunit tests/MyMonoLogTest.php --filter testWrite

$ vendor/bin/phpunit tests/MyMonoLogTest.php --filter testInfo
```



## 使用

```php
$MyMonoLog = new MyMonoLog();
$MyMonoLog->write(string $channel, string|array $message);
$MyMonoLog->channel(string $channel)->info(string|array $message);
```

